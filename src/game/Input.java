package game;

public class Input {
	
	public boolean down, clicked;
	public int x, y;

	public Input() {
	}

	public void toggle(boolean pressed) {
		if (pressed != down) {
			down = pressed;
		}
	}

	public void tick() {
	}
}