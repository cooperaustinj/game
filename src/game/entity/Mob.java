package game.entity;

import game.Game;
import game.gfx.Screen;

public class Mob extends Entity {

	public int maxLife = 100;
	public int currentLife = maxLife;
	public int power = 10;
	public int defense = 1;
	public int agility = 1;
	public int movespeed = 1;
	public boolean isMoving = false;
	public boolean canAttack;

	public Mob(Game game) {
		super(game);
	}

	public void render(Screen screen) {
	}

	public void tick() {
		super.tick();

		if (currentLife <= 0) {
			die();
		}
	}

	public void move() {
	}

	public void hurt(int damage) {
		currentLife -= damage;

	}

	public boolean isMoving(double dx, double dy) {
		if (Math.abs(dx) + Math.abs(dy) != 0) {
			return true;
		} else {
			return false;
		}
	}
}
