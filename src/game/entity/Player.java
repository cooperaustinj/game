package game.entity;

import game.Game;
import game.InputHandler;
import game.entity.particle.Spiral;
import game.gfx.Art;
import game.gfx.Screen;

public class Player extends Mob {

	private Game game;
	private InputHandler input;

	private int attackTime;
	private int cooldown;

	public Player(Game game, InputHandler input) {
		super(game);
		this.game = game;
		this.input = input;
		this.movespeed = 4;
		this.frames = 8;
		this.cooldown = 10;
		this.damage = 10;

		this.x = Game.WIDTH / 4 - SIZE / 2;
		this.y = Game.HEIGHT / 4 - SIZE / 2;
	}

	public void render(Screen screen) {
		if (isMoving) {
			if (frameTick / (currentFrame * frameTime + 1) == 1) {
				currentFrame++;
			}
			if (currentFrame > frames - 1) {
				currentFrame = 0;
			}
			screen.render(Art.sprites[currentFrame][2], x, y);
			frameTick++;
			if (frameTick > frameTime * frames) {
				frameTick = 0;
			}
		} else {
			if (frameTick / (currentFrame * frameTime + 1) == 1) {
				currentFrame++;
			}
			if (currentFrame > frames - 1) {
				currentFrame = 0;
			}
			screen.render(Art.sprites[currentFrame][3], x, y);
			frameTick++;
			if (frameTick > frameTime * frames) {
				frameTick = 0;
			}
		}
	}

	public void tick() {
		super.tick();

		if (attackTime <= 0) {
			canAttack = true;
		} else {
			attackTime--;
		}

		move();
		if (canAttack) {
			attack();
		}
	}

	public void move() {
		double dx = 0;
		double dy = 0;
		int xx = x;
		int yy = y;
		if (input.up.down) {
			dy -= movespeed;
		}
		if (input.down.down) {
			dy += movespeed;
		}
		if (input.left.down) {
			dx -= movespeed;
		}
		if (input.right.down) {
			dx += movespeed;
		}

		if (dx != 0 && dy != 0) {
			dx = (int) (dx * 0.75);
			dy = (int) (dy * 0.75);
		}

		isMoving = isMoving(dx, dy);

		x = (int) clampX(x + dx);
		y = (int) clampY(y + dy);
		
		for(Entity e : game.currentLevel.entities) {
			if(!e.equals(this) && e instanceof Enemy && intersects(e)) {
				x = xx;
				y = yy;
				dx = 0;
				dy = 0;
			}
		}
	}

	public void attack() {
		canAttack = false;
		attackTime = cooldown;
		if (input.atkUp.down) {
			game.currentLevel.addEntity(new Spiral(game, power, x, y - 10, 0, -10));
		} else if (input.atkDown.down) {
			canAttack = false;
			attackTime = cooldown;
			game.currentLevel.addEntity(new Spiral(game, power, x, y + 10, 0, 10));
		} else if (input.atkLeft.down) {
			canAttack = false;
			attackTime = cooldown;
			game.currentLevel.addEntity(new Spiral(game, power, x - 10, y, -10, 0));
		} else if (input.atkRight.down) {
			canAttack = false;
			attackTime = cooldown;
			game.currentLevel.addEntity(new Spiral(game, power, x + 10, y, 10, 0));
		} else {
			canAttack = true;
			attackTime = 0;
		}
	}

}
