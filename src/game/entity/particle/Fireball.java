package game.entity.particle;

import java.util.ArrayList;

import game.Game;
import game.entity.Entity;
import game.entity.Mob;
import game.entity.Player;
import game.gfx.Art;
import game.gfx.Screen;

public class Fireball extends Entity {

	private int lifetime;
	private double dx;
	private double dy;
	private int speed;
	private ArrayList<Mob> hit = new ArrayList<Mob>();

	public Fireball(Game game, int damage, int x, int y, double angle, int speed) {
		super(game);

		this.x = x;
		this.y = y;
		this.frames = 4;
		this.lifetime = frames * frameTime * 10;
		this.damage = damage;
		this.speed = speed;
		
//		double rand = Math.toRadians((random.nextInt(2) - 1 * 20));
//		this.dx = Math.cos(angle + rand) * speed;
//		this.dy = Math.sin(angle + rand) * speed;
		this.dx = Math.cos(angle) * speed;
		this.dy = Math.sin(angle) * speed;
	}

	public void render(Screen screen) {
		if (frameTick / (currentFrame * frameTime + 1) == 1) {
			currentFrame++;
		}
		if (currentFrame > frames - 1) {
			currentFrame = 0;
		}
		screen.render(Art.sprites[currentFrame + 4][1], x, y);
		frameTick++;
		if (frameTick > frameTime * frames) {
			frameTick = 0;
		}
	}

	public boolean intersects(Entity e) {
		return x < e.x + 18 && x + 18 > e.x && y < e.y + 18 && y + 18 > e.y;
	}

	public void tick() {
		super.tick();

		if (lifetime <= 0) {
			die();
		} else {
			lifetime--;
		}

		for (Entity e : game.currentLevel.entities) {
			if (e instanceof Player) {
				if (intersects(e) && !hit.contains(e)) {
					((Mob) e).hurt(damage);
					hit.add((Mob) e);
				}
			}
		}

		x += dx;
		y += dy;
	}

}
