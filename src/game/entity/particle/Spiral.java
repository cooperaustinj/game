package game.entity.particle;

import java.util.ArrayList;

import game.Game;
import game.entity.Enemy;
import game.entity.Entity;
import game.entity.Mob;
import game.gfx.Art;
import game.gfx.Screen;

public class Spiral extends Entity {

	private int lifetime;
	private int dx;
	private int dy;
	private ArrayList<Mob> hit = new ArrayList<Mob>();

	public Spiral(Game game, int damage, int x, int y, int dx, int dy) {
		super(game);

		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
		this.frames = 4;
		this.lifetime = frames * frameTime;
		this.damage = damage;
	}

	public void render(Screen screen) {
		if (frameTick / (currentFrame * frameTime + 1) == 1) {
			currentFrame++;
		}
		if (currentFrame > frames - 1) {
			currentFrame = 0;
		}
		screen.render(Art.sprites[currentFrame + 4][0], x, y);
		frameTick++;
		if (frameTick > frameTime * frames) {
			frameTick = 0;
		}
	}
	
	public boolean intersects(Entity e) {
		if(currentFrame <= 1) {
			return x < e.x + 11 && x + 11 > e.x && y < e.y + 11 && y + 11 > e.y;
		} else {
			return x < e.x + SIZE && x + SIZE > e.x && y < e.y + SIZE && y + SIZE > e.y;
		}
	}

	public void tick() {
		super.tick();

		if (lifetime <= 0) {
			die();
		} else {
			lifetime--;
		}

		for (Entity e : game.currentLevel.entities) {
			if (e instanceof Enemy) {
				if (intersects(e) && !hit.contains(e)) {
					((Mob) e).hurt(damage);
					hit.add((Mob) e);
				}
			}
		}

		x += dx;
		y += dy;
	}

}
