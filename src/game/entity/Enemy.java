package game.entity;

import game.Game;
import game.entity.particle.Fireball;
import game.gfx.Art;
import game.gfx.Screen;

public class Enemy extends Mob {

	private int cooldown = 0;
	private int idleTime = 0;

	public Enemy(Game game) {
		super(game);

		this.movespeed = 3;

		this.frames = 6;
		this.frameTime = 5;

		this.maxLife = 200;
		this.currentLife = 200;

		this.x = 50;
		this.y = 50;
	}

	public void render(Screen screen) {
		if (isMoving) {
			if (frameTick / (currentFrame * frameTime + 1) == 1) {
				currentFrame++;
			}
			if (currentFrame > frames - 1) {
				currentFrame = 0;
			}
			screen.render(Art.sprites[currentFrame][5], x, y);
			frameTick++;
			if (frameTick > frameTime * frames) {
				frameTick = 0;
			}
		} else {
			if (frameTick / (currentFrame * frameTime + 1) == 1) {
				currentFrame++;
			}
			if (currentFrame > frames - 1) {
				currentFrame = 0;
			}
			screen.render(Art.sprites[currentFrame][4], x, y);
			frameTick++;
			if (frameTick > frameTime * frames) {
				frameTick = 0;
			}
		}
	}

	public void tick() {
		super.tick();

		move();
		attack();
	}

	private void attack() {
		if (idleTime >= 200) {
			if (cooldown >= 2) {
				double angle = Math.atan2(game.player.y + SIZE / 2 - y, game.player.x + SIZE / 2 - x);
				game.currentLevel.addEntity(new Fireball(game, power, x, y, angle, 6));
				cooldown = 0;
				idleTime += random.nextInt(20) + 5;
			} else {
				cooldown += (System.nanoTime() % 4 == 0) ? 1 : 0;
			}
			if (idleTime >= 600) {
				idleTime = 0;
			}
		} else {
			idleTime += random.nextInt(10) + 1;
		}
	}

	public void move() {
		double dx = 0;
		double dy = 0;

		int xx = x;
		int yy = y;

		if (distanceTo(game.player) > 150) {
			double angle = Math.atan2(game.player.y - y, game.player.x - x);
			dx = Math.cos(angle) * movespeed;
			dy = Math.sin(angle) * movespeed;

			x += dx;
			y += dy;
		} else {
			if (distanceTo(game.player) < 100) {
				double angle = Math.atan2(game.player.y - y, game.player.x - x) + Math.PI;
				dx = Math.cos(angle) * movespeed;
				dy = Math.sin(angle) * movespeed;

				x = (int) clampX(x + dx);
				y = (int) clampY(y + dy);
			}
		}

		for (Entity e : game.currentLevel.entities) {
			if (!e.equals(this) && e instanceof Enemy && distanceTo(e) < 200) {
				x = xx;
				y = yy;
				dx = 0;
				dy = 0;
			}
		}

		isMoving = isMoving(dx, dy);
	}

	public void die() {
		removed = true;
		game.winCon++;
	}
}
