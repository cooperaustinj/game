package game.entity;

import java.util.Random;

import game.Game;
import game.gfx.Screen;

public class Entity {

	public static final int SIZE = 32;
	protected final Random random = new Random(System.nanoTime());;

	public int x;
	public int y;
	protected double dx = 0;
	protected double dy = 0;

	public int currentFrame = 0;
	public int frames;
	public int frameTick;
	public int frameTime = 5;
	public boolean removed = false;
	public int damage;

	public Game game;
	public int uniqueID;

	public Entity(Game game) {
		this.game = game;
	}

	public boolean intersects(Entity e) {
		return x < e.x + SIZE && x + SIZE > e.x && y < e.y + SIZE && y + SIZE > e.y;
	}

	public void render(Screen screen) {
	}

	public void tick() {
	}

	public void die() {
		removed = true;
	}

	public boolean equals(Object o) {
		if (o instanceof Entity) {
			return ((Entity) o).uniqueID == uniqueID;
		} else {
			return false;
		}
	}

	public void setUniqueID(int uniqueID) {
		this.uniqueID = uniqueID;
	}

	public double clampX(double x) {
		if (x < 0) {
			x = 0;
		}
		if (x + SIZE > Game.WIDTH) {
			x = Game.WIDTH - SIZE;
		}
		return x;
	}

	public double clampY(double y) {
		if (y < 0) {
			y = 0;
		}
		if (y + SIZE > Game.HEIGHT) {
			y = Game.HEIGHT - SIZE;
		}
		return y;
	}

	public double distanceTo(Entity e) {
		return Math.hypot((e.x + SIZE / 2) - (x + SIZE / 2), (e.y + SIZE / 2) - (y + SIZE / 2));
	}

	public double distanceTo(int x1, int y1) {
		return Math.hypot(x1 - (x + SIZE / 2), y1 - (y + SIZE / 2));
	}

}
