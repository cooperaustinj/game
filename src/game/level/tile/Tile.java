package game.level.tile;

import game.gfx.Art;
import game.gfx.Screen;
import game.level.Level;

public class Tile {

	public final static int SIZE = 32;
	
	public static Tile[] tiles = new Tile[256];
	
	public static Tile grass = new GrassTile(0);
	
	public final byte id;
	public int spriteX;
	public int spriteY;
	
	public Tile(int id) {
		this.id = (byte) id;
		tiles[id] = this;
	}
	
	public void render(Screen screen, Level level, int x, int y) {
		screen.render(Art.sprites[spriteY][spriteX], x * SIZE, y * SIZE);
	}
}
