package game.level;

import game.entity.Entity;
import game.entity.Player;
import game.gfx.Screen;
import game.level.tile.Tile;

import java.util.ArrayList;

public class Level {

	public int width;
	public int height;
	private int entityIDs = 0;

	public ArrayList<Entity> entities = new ArrayList<Entity>();

	public ArrayList<ArrayList<Tile>> tiles;

	public Level(ArrayList<ArrayList<Tile>> tiles, Player player) {
		this.tiles = tiles;

		this.width = tiles.size();
		this.height = tiles.get(0).size();

		entities = new ArrayList<Entity>();

		addEntity(player);
	}

	public Tile getTile(int x, int y) {
		return tiles.get(x).get(y);
	}

	public void setTile(int x, int y, Tile tile) {
		tiles.get(x).set(y, tile);
	}

	public void addEntity(Entity e) {
		entities.add(e);
		e.setUniqueID(entityIDs);
		entityIDs++;
	}

	public void removeEntity(Entity e) {
		entities.remove(e);
	}

	public void renderEntities(Screen screen) {
		for (Entity e : entities) {
			e.render(screen);
		}
	}

	public void tick() {
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			e.tick();
			if (e.removed) {
				entities.remove(e);
			}
		}
	}

}
