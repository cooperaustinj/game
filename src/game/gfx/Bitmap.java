package game.gfx;

import java.util.Arrays;

public class Bitmap {

	public int w;
	public int h;
	public int[] pixels;

	public Bitmap(int w, int h) {
		this.w = w;
		this.h = h;
		pixels = new int[w * h];

	}

	public void clear(int color) {
		Arrays.fill(pixels, color);
	}

	public void render(Bitmap bitmap, int x, int y) {
		int x0 = x;
		int x1 = x + bitmap.w;
		int y0 = y;
		int y1 = y + bitmap.h;

		// clamp values
		if (x0 < 0)
			x0 = 0;
		if (x1 > w)
			x1 = w;
		if (y0 < 0)
			y0 = 0;
		if (y1 > h)
			y1 = h;

		// absolute width
		int ww = x1 - x0;

		for (int yy = y0; yy < y1; yy++) {
			int tp = yy * w + x0;
			int sp = (yy - y) * bitmap.w + (x0 - x);
			tp -= sp;
			for (int xx = sp; xx < sp + ww; xx++) {
				int col = bitmap.pixels[xx];
				if (col < 0)
					pixels[tp + xx] = col;
			}
		}
	}
}
