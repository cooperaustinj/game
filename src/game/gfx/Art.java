package game.gfx;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import game.Game;

public class Art {
	
	public static final Bitmap[][] sprites = cut("/icons.png", 32, 32);

	private static Bitmap[][] cut(String string, int w, int h) {
		return cut(string, w, h, 0, 0);
	}

	private static Bitmap[][] cut(String string, int w, int h, int sx, int sy) {
		try {
			BufferedImage image = ImageIO.read(Game.class.getResourceAsStream(string));
			int xTiles = (image.getWidth() - sx) / w;
			int yTiles = (image.getHeight() - sy) / h;

			Bitmap[][] result = new Bitmap[xTiles][yTiles];

			for (int x = 0; x < xTiles; x++) {
				for (int y = 0; y < yTiles; y++) {
					result[x][y] = new Bitmap(w, h);
					image.getRGB(sx + x * w, sy + y * h, w, h, result[x][y].pixels, 0, w);
					
				}
			}
			return result;

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
