package game;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JFrame;

import game.entity.Enemy;
import game.entity.Player;
import game.gfx.Screen;
import game.level.Level;
import game.level.tile.Tile;

public class Game extends Canvas implements Runnable {

	private static final long serialVersionUID = 1L;
	public static final int F_WIDTH = 1280;
	public static final int F_HEIGHT = 768;
	public static final int WIDTH = F_WIDTH / 2;
	public static final int HEIGHT = F_HEIGHT / 2;
	public static final String GAME_NAME = "Game";
	public static final double NANOSECOND = 1000000000.0d;

	private Screen screen;
	public Level currentLevel;
	public InputHandler input = new InputHandler(this);

	public Player player;

	private boolean running = false;
	public int winCon = 0;

	public Game() {
		screen = new Screen(WIDTH, HEIGHT);
	}

	public void start() {
		running = true;
		new Thread(this).start();
	}

	public void stop() {
		running = false;
	}

	public void init() {

		player = new Player(this, input);

		ArrayList<ArrayList<Tile>> tiles;
		ArrayList<Tile> grasscol = new ArrayList<Tile>(Collections.nCopies(HEIGHT / Tile.SIZE, Tile.grass));
		tiles = new ArrayList<ArrayList<Tile>>(Collections.nCopies(WIDTH / Tile.SIZE, grasscol));
		currentLevel = new Level(tiles, player);

		Enemy e1 = new Enemy(this);
		Enemy e2 = new Enemy(this);
		e2.x = 500;
		currentLevel.addEntity(e1);
		currentLevel.addEntity(e2);
	}

	@Override
	public void run() {
		// limit FPS
		int fps = 0;
		int tick = 0;
		double fpsTimer = System.currentTimeMillis();
		double nsPerTick = NANOSECOND / 60;
		double then = System.nanoTime();
		double unprocessed = 0;
		init();

		while (running) {
			boolean canRender = false;
			double now = System.nanoTime();
			unprocessed += (now - then) / nsPerTick;
			then = now;
			while (unprocessed >= 1) {
				// update game
				tick++;
				tick();
				canRender = true;
				unprocessed--;
			}

			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// draw game
			if (canRender) {
				fps++;
				render();
			}

			if (System.currentTimeMillis() - fpsTimer > 1000) {
				System.out.printf("%d fps, %d tick\n", fps, tick);
				fps = 0;
				tick = 0;
				fpsTimer += 1000;
			}
		}
	}

	public void tick() {
		currentLevel.tick();
		if (player.removed) {
			running = false;
		}
	}

	public void render() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(2);
			requestFocus();
			return;
		}
		Graphics g = bs.getDrawGraphics();
		screen.clear(0);
		
		// begin rendering
		for (int x = 0; x < currentLevel.width; x++) {
			for (int y = 0; y < currentLevel.height; y++) {
				currentLevel.getTile(x, y).render(screen, currentLevel, x, y);
			}
		}

		currentLevel.renderEntities(screen);
		// end rendering
		
		if (player.removed) {
			screen.clear(0);
			g.setColor(Color.WHITE);
			g.drawImage(screen.image, 0, 0, getWidth(), getHeight(), null);
			g.setFont(new Font("Arial", Font.PLAIN, 42));
			g.drawString("Lost like Rock!", getWidth() / 2 - 120, getHeight() / 2 - 20);
		} else if (winCon == 2) {
			screen.clear(0);
			g.setColor(Color.WHITE);
			g.drawImage(screen.image, 0, 0, getWidth(), getHeight(), null);
			g.setFont(new Font("Arial", Font.PLAIN, 42));
			g.drawString("You won!", getWidth() / 2 - 80, getHeight() / 2 - 20);
		} else {
			g.drawImage(screen.image, 0, 0, getWidth(), getHeight(), null);
			g.setFont(new Font("Arial", Font.PLAIN, 42));
			g.setColor(Color.WHITE);
			g.drawString(String.format("HP: %s", player.currentLife), 10, 50);
		}
		g.dispose();
		bs.show();
	}

	public static void main(String[] args) {
		Game game = new Game();
		Dimension dimension = new Dimension(F_WIDTH, F_HEIGHT);
		game.setMaximumSize(dimension);
		game.setMinimumSize(dimension);
		game.setPreferredSize(dimension);
		game.setSize(dimension);

		JFrame frame = new JFrame();
		frame.setTitle(GAME_NAME);
		frame.add(game);
		frame.pack();
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		game.start();
	}
}
