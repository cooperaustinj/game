package game;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

public class InputHandler implements KeyListener, MouseListener {
	
	public List<Input> inputs = new ArrayList<Input>();

	public Input up = new Input();
	public Input down = new Input();
	public Input left = new Input();
	public Input right = new Input();

	public Input atkUp = new Input();
	public Input atkDown = new Input();
	public Input atkLeft = new Input();
	public Input atkRight = new Input();

	public Input lClick = new Input();
	public Input rClick = new Input();

	public void releaseAll() {
		for (int i = 0; i < inputs.size(); i++) {
			inputs.get(i).down = false;
		}
	}

	public void tick() {
		for (int i = 0; i < inputs.size(); i++) {
			inputs.get(i).tick();
		}
	}

	public InputHandler(Game game) {
		game.addKeyListener(this);
		game.addMouseListener(this);
		inputs.add(up);
		inputs.add(down);
		inputs.add(left);
		inputs.add(right);
		inputs.add(atkUp);
		inputs.add(atkDown);
		inputs.add(atkLeft);
		inputs.add(atkRight);
		inputs.add(lClick);
		inputs.add(rClick);
	}

	public void keyPressed(KeyEvent ke) {
		toggle(ke, true);
	}

	public void keyReleased(KeyEvent ke) {
		toggle(ke, false);
	}

	private void toggle(InputEvent ie, boolean pressed) {
		if (ie instanceof KeyEvent) {
			KeyEvent ke = (KeyEvent) ie;
			if (ke.getKeyCode() == KeyEvent.VK_W)
				up.toggle(pressed);
			if (ke.getKeyCode() == KeyEvent.VK_S)
				down.toggle(pressed);
			if (ke.getKeyCode() == KeyEvent.VK_A)
				left.toggle(pressed);
			if (ke.getKeyCode() == KeyEvent.VK_D)
				right.toggle(pressed);
			if (ke.getKeyCode() == KeyEvent.VK_UP)
				atkUp.toggle(pressed);
			if (ke.getKeyCode() == KeyEvent.VK_DOWN)
				atkDown.toggle(pressed);
			if (ke.getKeyCode() == KeyEvent.VK_LEFT)
				atkLeft.toggle(pressed);
			if (ke.getKeyCode() == KeyEvent.VK_RIGHT)
				atkRight.toggle(pressed);
		} else if (ie instanceof MouseEvent) {
			MouseEvent me = (MouseEvent) ie;
			if (me.getButton() == MouseEvent.BUTTON1) {
				lClick.toggle(pressed);
				lClick.x = me.getX();
				lClick.y = me.getY();
			}
			if (me.getButton() == MouseEvent.BUTTON2) {
				rClick.toggle(pressed);
				rClick.x = me.getX();
				rClick.y = me.getY();
			}
		}
	}

	public void keyTyped(KeyEvent ke) {
	}

	public void mouseClicked(MouseEvent me) {
	}

	public void mouseEntered(MouseEvent me) {
	}

	public void mouseExited(MouseEvent me) {
	}

	public void mousePressed(MouseEvent me) {
		toggle(me, true);
	}

	public void mouseReleased(MouseEvent me) {
		toggle(me, false);
	}
}